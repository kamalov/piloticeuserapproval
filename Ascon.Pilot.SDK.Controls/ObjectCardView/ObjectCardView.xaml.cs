﻿using System.Windows.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    /// <summary>
    /// Interaction logic for ObjectCardView.xaml
    /// </summary>
    public partial class ObjectCardView : UserControl
    {
        public ObjectCardView()
        {
            InitializeComponent();
        }
    }
}
