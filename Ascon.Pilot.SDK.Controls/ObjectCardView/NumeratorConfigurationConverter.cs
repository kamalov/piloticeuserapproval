﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using System.Windows.Markup;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class NumeratorConfigurationConverter : MarkupExtension, IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var configuration = values[0] as string;
            var parser = values[1] as IAttributeFormatParser;
            if (configuration == null || parser == null)
                return new List<INumeratorInfo>();

            IEnumerable<INumeratorInfo> collection;
            parser.TryParseNumeratorDeclaration(configuration, out collection);
            return collection.Select(x => new NumeratorInfoWrapper(x)).ToList();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}