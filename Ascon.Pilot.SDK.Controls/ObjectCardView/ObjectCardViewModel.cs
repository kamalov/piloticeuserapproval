﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class ObjectCardViewModel : PropertyChangedBase, IObjectCardAutoComplete, IDataErrorInfo
    {
        public event EventHandler IsValidInputChanged;

        protected readonly IObjectsRepository _repository;
        protected readonly IPilotDialogService _dialogService;
        protected readonly IAttributeFormatParser _attributeFormatParser;
        private IType _type;
        private readonly Dictionary<string, object> _serviceAttributes = new Dictionary<string, object>();
        private ObservableCollection<CardControlViewModel> _viewModelCollection;
        private bool _isValidInput;

        internal event EventHandler<EventArgs> ValuesChanged;

        public ObjectCardViewModel(IObjectsRepository repository, IPilotDialogService dialogService, IAttributeFormatParser attributeFormatParser)
        {
            _repository = repository;
            _dialogService = dialogService;
            _attributeFormatParser = attributeFormatParser;
            _viewModelCollection = new ObservableCollection<CardControlViewModel>();
        }

        public ObservableCollection<CardControlViewModel> ViewModelCollection
        {
            get => _viewModelCollection;
            set => _viewModelCollection = value;
        }

        public Dictionary<string, DValue> Values
        {
            get
            {
                var values = new Dictionary<string, DValue>();
                foreach (var cardControlViewModel in _viewModelCollection.Where(x => x.Value != null))
                {
                    values[cardControlViewModel.Attribute.Name] = DValue.GetDValue(cardControlViewModel.Value);
                }
                foreach (var serviceAttribute in _serviceAttributes)
                {
                    values[serviceAttribute.Key] = DValue.GetDValue(serviceAttribute.Value);
                }
                return values;
            }
        }

        public bool IsValidInput
        {
            get => _isValidInput;
            set
            {
                _isValidInput = value;
                RaiseIsValidInputChanged();
                NotifyOfPropertyChange(nameof(IsValidInput));
            }
        }

        private bool _isCardUpdated;
        public bool IsCardUpdated
        {
            get => _isCardUpdated;
            set
            {
                _isCardUpdated = value;
                InvalidateIsValidInput();
            }
        }

        public IAttributeFormatParser AttributeFormatParser => _attributeFormatParser;

        public bool IsDataChanged
        {
            get { return _viewModelCollection.Any(x => x.IsDataChanged); }
        }

        public IType Type
        {
            get => _type;
            set
            {
                _type = value;
                NotifyOfPropertyChange(nameof(Type));
            }
        }

        public void CreateCard(IType type)
        {
            Type = type;

            _viewModelCollection.Clear();

            if (type != null && type.Attributes.Count > 0)
            {
                var attributes = type.Attributes.OrderBy(u => u.DisplaySortOrder).Where(a => a.IsService == false).ToArray();
                for (var i = 0; i < attributes.Length; i++)
                {
                    var attribute = attributes[i];
                    var initValue = GetNewAttrValue(attribute);
                    var cardControlViewModel = CreateCardControlViewModel(attribute, type, initValue, false, false); //TODO editMode?

                    if (cardControlViewModel == null)
                        continue;

                    cardControlViewModel.PropertyChanged += (o, e) => InvalidateIsValidInput();
                    _viewModelCollection.Add(cardControlViewModel);
                }
            }

            InvalidateIsValidInput();
        }

        public virtual CardControlViewModel CreateCardControlViewModel(IAttribute attribute, IType type, object initValue, bool isEditMode, bool isReadOnlyAttribute)
        {
            switch (attribute.Type)
            {
                case AttributeType.OrgUnit:
                    return new OrgUnitCardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);

                default:
                    return new CardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);
            }
        }


        private void InvalidateIsValidInput()
        {
            var isDirty = _viewModelCollection.Any(viewModel => viewModel.Value != null);
            var hasValidationError = _viewModelCollection.Any(viewModel => viewModel.HasValidationError);
            var obligatoryFieldsAreNotEmpty = _viewModelCollection.All(viewModel => !viewModel.Attribute.IsObligatory || (viewModel.Value != null && !string.IsNullOrWhiteSpace(viewModel.Value.ToString())));
            IsValidInput = isDirty && !hasValidationError && obligatoryFieldsAreNotEmpty;
            NotifyOfPropertyChange(nameof(Values));
        }

        private void RaiseIsValidInputChanged()
        {
            IsValidInputChanged?.Invoke(this, EventArgs.Empty);
        }

        public void Fill(string attributeName, object value)
        {
            var viewModel = _viewModelCollection.FirstOrDefault(x => x.Attribute.Name == attributeName);
            if (viewModel != null)
            {
                // Отображаемый в карточке атрибут (кроме справочников)
                if (string.IsNullOrEmpty(viewModel.Attribute.Configuration))
                    viewModel.Value = value;
            }
            else
            {
                // Неотображаемый в карточке атрибут
                if (_type.Attributes.Any(x => x.Name == attributeName))
                    _serviceAttributes[attributeName] = value;
            }
            OnValuesChanged();
        }

        protected virtual void OnValuesChanged()
        {
            ValuesChanged?.Invoke(this, EventArgs.Empty);
            NotifyOfPropertyChange(nameof(Values));
        }

        private static object GetNewAttrValue(IAttribute attribute)
        {
            if (attribute.Type == AttributeType.DateTime)
                return DateTime.Now;
            return null;
        }

        #region validation

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                CardControlViewModel model = _viewModelCollection.First(c => c.Attribute.Name == columnName);
                if (!model.HasValidationError)
                    return model.Error;

                return String.Empty;
            }
        }
        #endregion
    }
}
