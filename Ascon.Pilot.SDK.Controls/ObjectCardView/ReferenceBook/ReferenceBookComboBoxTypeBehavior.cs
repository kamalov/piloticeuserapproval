﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ReferenceBook
{
    class ReferenceBookComboBoxTypeBehavior
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.RegisterAttached("ViewModel", typeof(CardControlViewModel), typeof(ReferenceBookComboBoxTypeBehavior), new PropertyMetadata(null, OnViewModelChanged));

        public static CardControlViewModel GetViewModel(DependencyObject obj)
        {
            return (CardControlViewModel)obj.GetValue(ViewModelProperty);
        }

        public static void SetViewModel(DependencyObject obj, CardControlViewModel value)
        {
            obj.SetValue(ViewModelProperty, value);
        }

        private static void OnViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = d as ComboBox;
            if (comboBox == null)
                throw new NotSupportedException();

            var viewModel = (CardControlViewModel)e.NewValue;

            comboBox.Loaded += (o, a) =>
            {
                comboBox.ItemsSource = GetCollection(viewModel);
                comboBox.IsTextSearchCaseSensitive = false;
            };
        }

        private static IEnumerable GetCollection(CardControlViewModel viewModel)
        {
            var repository = viewModel.Repository;
            var items = new List<ReferenceBookTypeItem>();
            var allTypes = repository.GetTypes().Where(t => !t.IsService && !t.IsDeleted);
            var types = viewModel.Attribute.Name == SystemAttributeNames.DOCUMENT_TEMPLATE_TYPE_ID
                ? allTypes.Where(t => t.IsMountable && t.HasFiles)
                : allTypes.Where(t => t.Kind == TypeKind.User);

            items.AddRange(types.Select(t => new ReferenceBookTypeItem(t)));
            return items;
        }
    }

    public class ReferenceBookTypeItem
    {
        public ReferenceBookTypeItem(IType type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            Title = type.Title;
            Id = type.Id;
            Categories = new List<string>();
        }

        public int Id { get; private set; }

        public string Title { get; private set; }
        public List<string> Categories { get; private set; }

        public override string ToString()
        {
            return Title;
        }
    }
}
