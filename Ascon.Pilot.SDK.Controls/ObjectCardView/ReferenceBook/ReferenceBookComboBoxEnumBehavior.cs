﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ReferenceBook
{
    class ReferenceBookComboBoxEnumBehavior
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.RegisterAttached("ViewModel", typeof(CardControlViewModel), typeof(ReferenceBookComboBoxEnumBehavior), new PropertyMetadata(null, OnViewModelChanged));

        public static CardControlViewModel GetViewModel(DependencyObject obj)
        {
            return (CardControlViewModel)obj.GetValue(ViewModelProperty);
        }

        public static void SetViewModel(DependencyObject obj, CardControlViewModel value)
        {
            obj.SetValue(ViewModelProperty, value);
        }

        private static void OnViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = d as ComboBox;
            if (comboBox == null)
                throw new NotSupportedException();

            var viewModel = (CardControlViewModel)e.NewValue;

            IReferenceBookConfiguration configuration;
            var success = viewModel.AttributeFormatParser.TryParseReferenceBookConfiguration(viewModel.Attribute.Configuration, out configuration);
           
            if(!success)
                return;

            comboBox.IsReadOnly = !configuration.IsEditable;

            comboBox.DropDownOpened += (o, a) =>
            {
                comboBox.ItemsSource = configuration.Values;
            };
        }
    }
}
