﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ReferenceBook
{
    class ReferenceBookComboBoxOrgUnitBehavior
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.RegisterAttached("ViewModel", typeof(CardControlViewModel), typeof(ReferenceBookComboBoxOrgUnitBehavior), new PropertyMetadata(null, OnViewModelChanged));

        public static CardControlViewModel GetViewModel(DependencyObject obj)
        {
            return (CardControlViewModel)obj.GetValue(ViewModelProperty);
        }

        public static void SetViewModel(DependencyObject obj, CardControlViewModel value)
        {
            obj.SetValue(ViewModelProperty, value);
        }

        private static void OnViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = d as ComboBox;
            if (comboBox == null)
                throw new NotSupportedException();

            var viewModel = (CardControlViewModel)e.NewValue;

            IReferenceBookConfiguration configuration;
            var success = viewModel.AttributeFormatParser.TryParseReferenceBookConfiguration(viewModel.Attribute.Configuration, out configuration);

            if (!success)
                return;

            comboBox.IsReadOnly = !configuration.IsEditable;

            comboBox.DropDownOpened += (o, a) =>
            {
                comboBox.ItemsSource = GetCollectionView(viewModel.Repository);
            };
        }

        private static IEnumerable GetCollectionView(IObjectsRepository repository)
        {
            var items = new List<ReferenceBookOrgUnitItem>();
            var root = repository.GetOrganisationUnits().FirstOrDefault();
            BuildReferenceBookItems(repository, GetSortedChildren(root, repository), items, new string[] { });

            if (!items.Any())
                return null;

            var lcv = new ListCollectionView(items);
            for (var i = 0; i < items.Max(x => x.Categories.Count); i++)
            {
                lcv.GroupDescriptions.Add(new PropertyGroupDescription($"Categories[{i}]"));
            }
            return lcv;
        }

        private static IEnumerable<IOrganisationUnit> GetSortedChildren(IOrganisationUnit unit, IObjectsRepository repository)
        {
            var children = unit.Children.Select(repository.GetOrganisationUnit).ToList();
            return children.OrderBy(c => c.IsPosition == false).Select(e => e);
        }

        private static void BuildReferenceBookItems(IObjectsRepository repository, IEnumerable<IOrganisationUnit> positions, ICollection<ReferenceBookOrgUnitItem> items, IList<string> categories)
        {
            var people = repository.GetPeople().ToList();

            foreach (var unit in positions)
            {
                if (unit.IsPosition)
                {
                    var person = people.Where(p => p.Positions.Select(m => m.Position).Contains(unit.Id))
                        .OrderBy(o => o.Positions.First(x => x.Position == unit.Id).Order)
                        .FirstOrDefault();

                    items.Add(person == null
                        ? new ReferenceBookOrgUnitItem(unit.Title, categories.ToList())
                        : new ReferenceBookOrgUnitItem(person.DisplayName, categories.ToList()));
                }
                else
                {
                    BuildReferenceBookItems(repository, GetSortedChildren(unit, repository), items, categories.Union(new[] { unit.Title }).ToList());
                }
            }
        }
    }

    public class ReferenceBookOrgUnitItem
    {
        public string Title { get; private set; }
        public List<string> Categories { get; private set; }

        public ReferenceBookOrgUnitItem(string title, IEnumerable<string> categories)
        {
            if (title == null)
                throw new ArgumentNullException(nameof(title));
            if (categories == null)
                throw new ArgumentNullException(nameof(categories));
            Title = title;
            Categories = new List<string>(categories);
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
