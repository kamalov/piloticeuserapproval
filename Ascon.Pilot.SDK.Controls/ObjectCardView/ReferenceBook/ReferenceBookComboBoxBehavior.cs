﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Ascon.Pilot.SDK.Controls.Tools;
using Ascon.Pilot.Theme.Tools;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ReferenceBook
{
    static class ReferenceBookComboBoxBehavior
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.RegisterAttached("ViewModel", typeof(CardControlViewModel), typeof(ReferenceBookComboBoxBehavior), new PropertyMetadata(null, OnViewModelChanged));

        public static CardControlViewModel GetViewModel(DependencyObject obj)
        {
            return (CardControlViewModel)obj.GetValue(ViewModelProperty);
        }

        public static void SetViewModel(DependencyObject obj, CardControlViewModel value)
        {
            obj.SetValue(ViewModelProperty, value);
        }

        private static void OnViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = d as ComboBox;
            if (comboBox == null)
                throw new NotSupportedException();

            var viewModel = (CardControlViewModel)e.NewValue;
            IReferenceBookConfiguration configuration;
            var success = viewModel.AttributeFormatParser.TryParseReferenceBookConfiguration(viewModel.Attribute.Configuration, out configuration);
           
            if(!success)
                return;

            comboBox.IsReadOnly = !configuration.IsEditable;

            comboBox.DropDownOpened += (o, a) =>
            {
                ComboBoxEx.SetIsLoading(comboBox, true);
                var loader = new ReferenceBookLoader(viewModel.Repository);
                loader.Completed += (l, args) =>
                {
                    comboBox.ItemsSource = GetCollectionView(configuration.Source, args.Objects, configuration.StringFormat, configuration.ElementsTypes.ToList(), viewModel.AttributeFormatParser);
                    ComboBoxEx.SetIsLoading(comboBox, false);
                };
                loader.Load(configuration.Source);
            };
            comboBox.SelectionChanged += (o, a) =>
            {
                var selectedItem = comboBox.SelectedItem as ReferenceBookItem;
                if (selectedItem != null)
                {
                    foreach (var attribute in selectedItem.Object.Attributes)
                    {
                        if (viewModel.Attribute.Name != attribute.Key)
                            viewModel.AutoComplete.Fill(attribute.Key, attribute.Value);
                    }
                }
            };
        }

        private static IEnumerable GetCollectionView(Guid sourceId, Dictionary<Guid, IDataObject> objects, string stringFormat, IList<string> typesToShow, IAttributeFormatParser attributeFormatParser)
        {
            var items = new List<ReferenceBookItem>();
            BuildReferenceBookItems(sourceId, objects, new string[0], stringFormat, items, typesToShow, attributeFormatParser);
            if (!items.Any())
                return null;

            var lcv = new ListCollectionView(items);
            for (int i = 0; i < items.Max(x => x.Categories.Count); i++)
            {
                lcv.GroupDescriptions.Add(new PropertyGroupDescription(string.Format("Categories[{0}]", i)));
            }
            return lcv;
        }

        private static void BuildReferenceBookItems(Guid parentId, Dictionary<Guid, IDataObject> objects, IList<string> categories, string stringFormat, List<ReferenceBookItem> result, IList<string> typesToShow, IAttributeFormatParser attributeFormatParser)
        {
            var children = GetSortedChildren(parentId, objects, typesToShow);
            if (children == null)
                return;

            foreach (var child in children)
            {
                var title = child.DisplayName;
                if (IsLeaf(child))
                {
                    var formattedTitle = string.IsNullOrEmpty(stringFormat)
                        ? title
                        : attributeFormatParser.AttributesFormat(stringFormat, child.Attributes.ToDictionary(x=>x.Key, x=>x.Value));
                    result.Add(new ReferenceBookItem(child, formattedTitle, categories.ToList()));
                }
                else
                {
                    BuildReferenceBookItems(child.Id, objects, categories.Union(new[] { title }).ToList(), stringFormat, result, typesToShow, attributeFormatParser);
                }
            }
        }

        private static IEnumerable<IDataObject> GetSortedChildren(Guid parentId, Dictionary<Guid, IDataObject> objects, IList<string> typesToShow)
        {
            IDataObject parent;
            if (!objects.TryGetValue(parentId, out parent))
                return null;

            var children = parent.Children
                .Where(objects.ContainsKey)
                .Select(x => objects[x]).Where(x => !typesToShow.Any() || !IsLeaf(x) || typesToShow.Contains(x.Type.Name))
                .ToList();
            children.Sort(new ObjectComparer());
            return children;
        }

        private static bool IsLeaf(IDataObject obj)
        {
            return !obj.Type.Children.Any();
        }
    }

    class ReferenceBookItem
    {
        public IDataObject Object { get; private set; }
        public string Title { get; private set; }
        public List<string> Categories { get; private set; }

        public ReferenceBookItem(IDataObject obj, string title, IEnumerable<string> categories)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            if (title == null)
                throw new ArgumentNullException("title");
            if (categories == null)
                throw new ArgumentNullException("categories");
            Object = obj;
            Title = title;
            Categories = new List<string>(categories);
        }

        public override string ToString()
        {
            return Title;
        }
    }

    class ObjectComparer : IComparer<IDataObject>, IComparer
    {
        private readonly int _direction;

        public ObjectComparer(bool ascending = true)
        {
            _direction = ascending ? 1 : -1;
        }

        public int Compare(IDataObject x, IDataObject y)
        {
            var result = x.Type.Sort.CompareTo(y.Type.Sort);
            if (result != 0)
                return result;

            result = NaturalComparer.Compare(x.DisplayName, y.DisplayName);
            if (result != 0)
                return _direction * result;

            return _direction * (x.GetHashCode() - y.GetHashCode());
        }

        public int Compare(object x, object y)
        {
            var obj1 = x as IDataObject;
            var obj2 = y as IDataObject;
            return Compare(obj1, obj2);
        }
    }
}
