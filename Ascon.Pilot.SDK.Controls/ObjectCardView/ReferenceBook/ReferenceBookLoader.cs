﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ReferenceBook
{
    class ReferenceBookLoader : IObserver<IDataObject>
    {
        public class ReferenceBookLoadedEventArgs : EventArgs
        {
            public Dictionary<Guid, IDataObject> Objects { get; private set; }

            public ReferenceBookLoadedEventArgs(Dictionary<Guid, IDataObject> objects)
            {
                Objects = objects;
            }
        }

        public event EventHandler<ReferenceBookLoadedEventArgs> Completed;

        private Guid _sourceId;
        private readonly IObjectsRepository _repository;
        private readonly Dictionary<Guid, IDataObject> _loaded = new Dictionary<Guid, IDataObject>();
        private readonly HashSet<Guid> _toLoad = new HashSet<Guid>();
        private readonly ListViewTypeFilter _filter;

        public ReferenceBookLoader(IObjectsRepository repository)
        {
            _repository = repository;
            _filter = new ListViewTypeFilter(() => _repository);
        }

        public void Load(Guid sourceId)
        {
            _sourceId = sourceId;
            StartLoad();
        }

        private void StartLoad()
        {
            _toLoad.Add(_sourceId);
            _repository.SubscribeObjects(new List<Guid> { _sourceId }).Subscribe(this);
        }

        //TODO
        public void OnNext(IDataObject value)
        {
            if (_loaded.ContainsKey(value.Id))
                return;

            var newObject = value;
            var newObjectMatches = _filter.Matches(newObject.Type.Id);

            if (newObjectMatches)
                _loaded.Add(value.Id, newObject);
            
            _toLoad.Remove(value.Id);

            if (newObjectMatches && newObject.Children.Any())
            {
                var children = newObject.Children.ToList();

                foreach (var child in children)
                    _toLoad.Add(child);

                _repository.SubscribeObjects(children).Subscribe(this);
            }

            if (!_toLoad.Any())
                OnCompleted();
        }

        public void OnError(Exception error)
        {
            OnCompleted();
        }

        public void OnCompleted()
        {
            Completed?.Invoke(this, new ReferenceBookLoadedEventArgs(_loaded));
        }
    }
}
