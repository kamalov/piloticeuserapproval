﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class CardEditorTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var viewModel = (CardControlViewModel)item;
            if (viewModel == null)
                return null;

            switch (viewModel.Attribute.Type)
            {
                case AttributeType.String:
                    if (!string.IsNullOrEmpty(viewModel.Attribute.Configuration) && !viewModel.IsReadOnly)
                    {
                        IReferenceBookConfiguration configuration;
                        var success = viewModel.AttributeFormatParser.TryParseReferenceBookConfiguration(viewModel.Attribute.Configuration, out configuration);

                        if (!success)
                            return CardEditorTemplates.Instance.StringEditTemplate;

                        switch (configuration.EditorType)
                        {
                            case RefBookEditorType.ComboBox:
                                {
                                    switch (configuration.Kind)
                                    {
                                        case RefBookKind.Enum:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxEnumEditTemplate;
                                        case RefBookKind.Object:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxEditTemplate;
                                        case RefBookKind.OrgUnit:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxOrgUnitEditTemplate;
                                        case RefBookKind.Type:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxTypeEditTemplate;
                                        default:
                                            throw new ArgumentOutOfRangeException();
                                    }
                                }
                            case RefBookEditorType.Dialog:
                                return CardEditorTemplates.Instance.ReferenceBookDialogEditTemplate;
                            default:
                                throw new NotSupportedException("ReferenceBookAttributeEditorType." + configuration.EditorType);
                        }
                    }
                    return CardEditorTemplates.Instance.StringEditTemplate;
                case AttributeType.Integer:
                    return CardEditorTemplates.Instance.LongEditTemplate;
                case AttributeType.Double:
                    return CardEditorTemplates.Instance.DoubleEditTemplate;
                case AttributeType.DateTime:
                    return CardEditorTemplates.Instance.DateEditTemplate;
                case AttributeType.Decimal:
                    return CardEditorTemplates.Instance.DecimalEditTemplate;
                case AttributeType.Numerator:
                    return viewModel.EditMode
                        ? CardEditorTemplates.Instance.NumeratorStringEditTemplate
                        : CardEditorTemplates.Instance.NumeratorEditTemplate;
                case AttributeType.UserState:
                    return CardEditorTemplates.Instance.StateEditTemplate;
                case AttributeType.OrgUnit:
                    return CardEditorTemplates.Instance.OrgUnitEditTemplate;
                default:
                    throw new NotSupportedException();
            }
        }
    }

    partial class CardEditorTemplates
    {
        internal static readonly CardEditorTemplates Instance = new CardEditorTemplates();

        public DataTemplate DateEditTemplate => (DataTemplate)this["DateEditTemplate"];

        public DataTemplate LongEditTemplate => (DataTemplate)this["LongEditTemplate"];

        public DataTemplate DoubleEditTemplate => (DataTemplate)this["DoubleEditTemplate"];

        public DataTemplate DecimalEditTemplate => (DataTemplate)this["DecimalEditTemplate"];

        public DataTemplate StringEditTemplate => (DataTemplate)this["StringEditTemplate"];

        public DataTemplate NumeratorEditTemplate => (DataTemplate)this["NumeratorEditTemplate"];

        public DataTemplate NumeratorStringEditTemplate => (DataTemplate)this["NumeratorStringEditTemplate"];

        public DataTemplate ReferenceBookDialogEditTemplate => (DataTemplate)this["ReferenceBookDialogEditTemplate"];

        public DataTemplate ReferenceBookComboBoxEditTemplate => (DataTemplate)this["ReferenceBookComboBoxEditTemplate"];

        public DataTemplate ReferenceBookComboBoxEnumEditTemplate => (DataTemplate)this["ReferenceBookComboBoxEnumEditTemplate"];

        public DataTemplate ReferenceBookComboBoxOrgUnitEditTemplate => (DataTemplate)this["ReferenceBookComboBoxOrgUnitEditTemplate"];

        public DataTemplate ReferenceBookComboBoxTypeEditTemplate => (DataTemplate)this["ReferenceBookComboBoxTypeEditTemplate"];

        public DataTemplate StateEditTemplate => (DataTemplate)this["StateEditTemplate"];

        public DataTemplate OrgUnitEditTemplate => (DataTemplate)this["OrgUnitEditTemplate"];

        private CardEditorTemplates()
        {
            InitializeComponent();
        }
    }
}
