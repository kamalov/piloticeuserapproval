﻿namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public interface IObjectCardAutoComplete
    {
        void Fill(string attributeName, object value);
    }
}
