﻿using System;
using System.Globalization;
using System.Linq;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class DValue : IEquatable<string>, IEquatable<long>, IEquatable<int>, IEquatable<double>, IEquatable<DateTime>, IEquatable<decimal>, IEquatable<Guid>
    {
        public object Value { get; set; }

        public string StrValue
        {
            get { return Value as String; }
            set { Value = value; }
        }

        public long? IntValue
        {
            get { return Value as long?; }
            set { Value = value; }
        }

        public double? DoubleValue
        {
            get { return Value as double?; }
            set { Value = value; }
        }

        public DateTime? DateValue
        {
            get { return Value as DateTime?; }
            set { Value = value; }
        }

        public string[] ArrayValue
        {
            get { return Value as string[]; }
            set { Value = value; }
        }

        public decimal? DecimalValue
        {
            get { return Value as decimal?; }
            set { Value = value; }
        }

        public Guid? GuidValue
        {
            get { return Value as Guid?; }
            set { Value = value; }
        }

        public int[] ArrayIntValue
        {
            get { return Value as int[]; }
            set { Value = value; }
        }

        public override int GetHashCode()
        {
            if (Value == null)
                return 0;
            return Value.GetHashCode();
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(this, other))
                return true;
            if (other is DValue)
                return Equals(other as DValue);
            return Equals(Value, other);
        }

        public bool Equals(DValue other)
        {
            if (ReferenceEquals(this, other))
                return true;
            if (other == null)
                return false;

            if (ArrayIntValue != null && other.ArrayIntValue != null)
            {
                return ArrayIntValue.SequenceEqual(other.ArrayIntValue);
            }

            if (ArrayValue != null && other.ArrayValue != null)
            {
                return ArrayValue.SequenceEqual(other.ArrayValue);
            }

            return Equals(Value, other.Value);
        }

        public static implicit operator DValue(long value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(double value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(String value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(String[] value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(int[] value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(DateTime value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(decimal value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator DValue(Guid value)
        {
            return new DValue { Value = value };
        }

        public static implicit operator long(DValue value)
        {
            return (long)value.Value;
        }

        public static implicit operator double(DValue value)
        {
            return (double)value.Value;
        }

        public static implicit operator string(DValue value)
        {
            return (string)value?.Value;
        }

        public static implicit operator string[] (DValue value)
        {
            return (string[])value?.Value;
        }

        public static implicit operator int[] (DValue value)
        {
            return (int[])value?.Value;
        }

        public static implicit operator DateTime(DValue value)
        {
            return (DateTime)value.Value;
        }

        public static implicit operator decimal(DValue value)
        {
            return (decimal)value.Value;
        }

        public static implicit operator Guid(DValue value)
        {
            return (Guid)value.Value;
        }

        public bool IsArray
        {
            get { return Value != null && Value.GetType().IsArray; }
        }

        public DValue Clone()
        {
            return new DValue { Value = Value };
        }

        public bool Equals(string other) => Value.Equals(other);
        public bool Equals(int other) => Value.Equals((long)other);
        public bool Equals(long other) => Value.Equals(other);
        public bool Equals(double other) => Value.Equals(other);
        public bool Equals(decimal other) => Value.Equals(other);
        public bool Equals(DateTime other) => Value.Equals(other);
        public bool Equals(Guid other) => Value.Equals(other);

        public override string ToString()
        {
            if (StrValue != null)
                return StrValue;
            if (IntValue != null)
                return IntValue.ToString();
            if (DoubleValue != null)
                return DoubleValue.ToString();
            if (DateValue != null)
                return DateValue.Value.ToShortDateString();
            if (DecimalValue != null)
                return DecimalValue.Value.ToString(CultureInfo.InvariantCulture);
            if (ArrayValue != null)
                return string.Join(", ", ArrayValue);
            if (ArrayIntValue != null)
                return string.Join(", ", ArrayIntValue);
            if (GuidValue != null)
                return GuidValue.Value.ToString();
            return string.Empty;
        }

        public static DValue GetDValue(object value)
        {
            if (value == null)
                return new DValue();
            if (value is int)
                return (int)value;
            if (value is long)
                return (long)value;
            if (value is double)
                return (double)value;
            if (value is string)
                return (string)value;
            if (value is DateTime)
                return (DateTime)value;
            if (value is decimal)
                return (decimal)value;
            if (value is string[])
                return (string[])value;
            if (value is int[])
                return (int[])value;
            if (value is Guid)
                return (Guid)value;

            throw new Exception($"Error converting attribute value [{value}] to DValue");
        }
    }
}
