﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.States
{
    public static class StateConfigurationSerializer
    {
        public static string Serialize(UserStatesConfiguration input)
        {
            var serializer = new XmlSerializer(typeof(UserStatesConfiguration), GetKnownTypes());
            using (var stream = new MemoryStream())
            {
                serializer.Serialize(stream, input);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        public static UserStatesConfiguration Deserialize(string input)
        {
            if (string.IsNullOrEmpty(input))
                return new UserStatesConfiguration();

            var serializer = new XmlSerializer(typeof(UserStatesConfiguration), GetKnownTypes());
            var bytes = Encoding.UTF8.GetBytes(input);
            using (var stream = new MemoryStream(bytes))
            {
                try
                {
                    return (UserStatesConfiguration)serializer.Deserialize(stream);
                }
                catch (Exception e)
                {
                    return new UserStatesConfiguration();
                }

            }
        }

        private static Type[] GetKnownTypes()
        {
            return new[] { typeof(UserStatesConfiguration) };
        }
    }

    public class UserStatesConfiguration
    {
        public List<string> UserStates { get; set; }

        public UserStatesConfiguration()
        {
            UserStates = new List<string>();
        }
    }
}
