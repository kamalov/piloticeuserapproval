﻿using System.Collections.Generic;
using System.Windows.Media;
using Ascon.Pilot.Themes;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.States
{
    public static class UserStateColorExtensions
    {
        private static readonly UserStateColorSchemeFactory Factory = new UserStateColorSchemeFactory();
        public static Color GetColor(this UserStateColorNames color, ThemeNames theme = ThemeNames.Jedi)
        {
            var scheme = Factory.GetStateColorSheme(theme);
            return scheme.GetColor(color);
        }
    }

    class UserStateColorSchemeFactory
    {
        private readonly Dictionary<ThemeNames, IUserStateColorScheme> _colorShemes = new Dictionary<ThemeNames, IUserStateColorScheme>()
        {
            {ThemeNames.Jedi, new UserStateLightColorScheme() },
            {ThemeNames.Sith, new UserStateDarkColorScheme() }
        };

        public IUserStateColorScheme GetStateColorSheme(ThemeNames theme)
        {
            return _colorShemes[theme];
        }
    }

    interface IUserStateColorScheme
    {
        Color GetColor(UserStateColorNames color);
    }

    class UserStateLightColorScheme : IUserStateColorScheme
    {
        private readonly Dictionary<UserStateColorNames, Color> _colors = new Dictionary<UserStateColorNames, Color>
        {
            { UserStateColorNames.None, Colors.Transparent },
            { UserStateColorNames.Color1, Color.FromArgb(255, 186, 186, 186) },
            { UserStateColorNames.Color2, Color.FromArgb(255, 183, 226, 129) },
            { UserStateColorNames.Color3, Color.FromArgb(255, 146, 225, 213) },
            { UserStateColorNames.Color4, Color.FromArgb(255, 166, 224, 252) },
            { UserStateColorNames.Color5, Color.FromArgb(255, 255, 200, 234) },
            { UserStateColorNames.Color6, Color.FromArgb(255, 254, 159, 121) },
            { UserStateColorNames.Color7, Color.FromArgb(255, 224, 195, 120) },
        }; 

        public Color GetColor(UserStateColorNames color)
        {
            return _colors[color];
        }
    }

    class UserStateDarkColorScheme : IUserStateColorScheme
    {
        private readonly Dictionary<UserStateColorNames, Color> _colors = new Dictionary<UserStateColorNames, Color>
        {
            { UserStateColorNames.None, Colors.Transparent },
            { UserStateColorNames.Color1, Color.FromArgb(255, 77, 77, 77) },
            { UserStateColorNames.Color2, Color.FromArgb(255, 36, 101, 18) },
            { UserStateColorNames.Color3, Color.FromArgb(255, 0, 102, 94) },
            { UserStateColorNames.Color4, Color.FromArgb(255, 0, 80, 161) },
            { UserStateColorNames.Color5, Color.FromArgb(255, 144, 0, 82) },
            { UserStateColorNames.Color6, Color.FromArgb(255, 142, 22, 0) },
            { UserStateColorNames.Color7, Color.FromArgb(255, 85, 48, 0) },
        };

        public Color GetColor(UserStateColorNames color)
        {
            return _colors[color];
        }
    }
}
