﻿using System;
using System.Windows.Media;
using Ascon.Pilot.Theme.ColorScheme;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.States
{
    class StateItem : PropertyChangedBase
    {
        public StateItem(IUserState state)
        {
            if (state == null)
                return;

            Id = state.Id;
            Title = state.Title;
            Icon = GetIcon(state);
            Background = GetBrush(state.Color);
            Name = state.Name;
        }

        public string Name { get; }

        public Guid? Id { get; }

        public string Title { get; protected set; }

        public ImageSource Icon { get; }

        public SolidColorBrush Background { get; }

        public override string ToString()
        {
            return Title;
        }

        private ImageSource GetIcon(IUserState state)
        {
            return UserStateIconConverter.Convert(state);
        }

        private SolidColorBrush GetBrush(UserStateColorNames colors)
        {
            return colors == UserStateColorNames.None ? null : new SolidColorBrush(colors.GetColor(ColorScheme.GetCurrentTheme()));
        }
    }

    class EmptyStateItem : StateItem
    {
        public EmptyStateItem() : base(null)
        {
            Title = "(нет)";
        }
    }
}
