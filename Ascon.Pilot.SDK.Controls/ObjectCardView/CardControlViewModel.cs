﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.Theme.Tools;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class CardControlViewModel : PropertyChangedBase, ISingleValidationErrorHandler
    {
        protected readonly IType _type;
        protected readonly IPilotDialogService _dialogService;
        protected readonly IObjectsRepository _repository;
        protected readonly IAttributeFormatParser _attributeFormatParser;
        private object _value;
        private bool _hasValidationError;
        private bool _isReadOnly;
        private readonly DValue _originalValue;
        private string _format;
        private string _culture;

        public CardControlViewModel(IAttribute attribute, IType type, object initValue, bool isReadOnly, 
            IPilotDialogService dialogService, IObjectsRepository repository, IObjectCardAutoComplete autoComplete, 
            IAttributeFormatParser attributeFormatParser, bool editMode)
        {
            if (dialogService == null)
                throw new ArgumentNullException(nameof(dialogService));
            if (repository == null) 
                throw new ArgumentNullException(nameof(repository));
            if (attributeFormatParser == null)
                throw new ArgumentNullException(nameof(attributeFormatParser));
            if (autoComplete == null)
                throw new ArgumentNullException(nameof(autoComplete));
            if (attribute == null)
                throw new ArgumentNullException(nameof(attribute));

            _type = type;
            _dialogService = dialogService;
            _repository = repository;
            _attributeFormatParser = attributeFormatParser;
            AutoComplete = autoComplete;
            EditMode = editMode;

            Attribute = attribute;
            _originalValue = DValue.GetDValue(initValue);
            _value = initValue;
            IsReadOnly = isReadOnly;

            Format = _attributeFormatParser.GetAttributeFormat(Attribute.Configuration);
            if (_originalValue != null && (_originalValue.DateValue != null && Format == null))
                Format = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

            Culture = _attributeFormatParser.GetAttributeFormatCulture(Attribute.Configuration);
        }

        private static bool IsReadOnlyReferenceBookAttribute(IAttribute attribute, IAttributeFormatParser attributeFormatParser)
        {
            if (attribute.Configuration == null)
                return false;

            var success = attributeFormatParser.TryParseReferenceBookConfiguration(attribute.Configuration, out var attributeConfiguration);
            return success && !attributeConfiguration.IsEditable;
        }

        public bool IsNotEditableReferenceBookAttribute => IsReadOnlyReferenceBookAttribute(Attribute, _attributeFormatParser);

        public IObjectsRepository Repository => _repository;

        public IObjectCardAutoComplete AutoComplete { get; }

        public bool IsDataChanged
        {
            get
            {
                var dValue = DValue.GetDValue(Value);
                return !_originalValue.Equals(dValue);
            }
        }

        public IAttribute Attribute { get; }

        public IAttributeFormatParser AttributeFormatParser => _attributeFormatParser;

        public bool IsReadOnly
        {
            get => _isReadOnly;
            set
            {
                _isReadOnly = value;
                NotifyOfPropertyChange(nameof(IsReadOnly));
            }
        }

        public object Value
        {
            get => _value;
            set
            {
                _value = value;
                NotifyOfPropertyChange(nameof(Value));
            }
        }

        public string Format
        {
            get => _format;
            set
            {
                _format = value;
                NotifyOfPropertyChange(nameof(Format));
            }
        }

        public string Culture
        {
            get => _culture;
            set
            {
                _culture = value;
                NotifyOfPropertyChange(nameof(Culture));

            }
        }

        public bool HasValidationError
        {
            get => _hasValidationError;
            set
            {
                _hasValidationError = value;
                NotifyOfPropertyChange(nameof(HasValidationError));
            }
        }

        public string Error { get; set; }

        public ICommand ShowReferenceBookViewCommand => new DelegateCommand(ShowReferenceBookView);

        public bool EditMode { get; }

        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();

        private void ShowReferenceBookView()
        {
            var success = _attributeFormatParser.TryParseReferenceBookConfiguration(Attribute.Configuration, out var configuration);
            if (!success)
                return;

            if (configuration.Kind == RefBookKind.OrgUnit)
            {
                var dialogOptions = _dialogService.NewOptions()
                    .WithAllowChecking(true)
                    .WithAllowMultiSelect(true)
                    .WithOkButtonCaption("Ok")
                    .WithParentWindow(GetActiveWindow());

                var selectedPositions = _dialogService.ShowPositionSelectorDialog(dialogOptions);

                var positionsTitles = selectedPositions.Select(x => GetEffectiveAttributeTitle(x.Id));
                Value = string.Join("; ", positionsTitles);
                return;
            }

            var options = _dialogService.NewOptions();
            options.WithAllowChecking(true);
            options.WithParentWindow(GetActiveWindow());

            var checkedNodes = _dialogService.ShowReferenceBookDialog(Attribute.Configuration, _type.Name + "-" + Attribute.Name, options).ToList();
            if (!checkedNodes.Any())
                return;

            var titles = checkedNodes.Select(n => GetEffectiveAttributeTitle(n, configuration.StringFormat));
            Value = string.Join("; ", titles);

            if (checkedNodes.Count() == 1)
                foreach (var attribute in checkedNodes.First().Attributes)
                    AutoComplete.Fill(attribute.Key, attribute.Value);
        }

        private string GetEffectiveAttributeTitle(IDataObject obj, string stringFormat)
        {
            var formattedTitle = string.IsNullOrEmpty(stringFormat)
                ? obj.DisplayName
                : _attributeFormatParser.AttributesFormat(stringFormat, obj.Attributes.ToDictionary(x => x.Key, x => x.Value));
            return formattedTitle;
        }

        private string GetEffectiveAttributeTitle(int orgUnitId)
        {
            var people = _repository.GetPeople().ToList();
            var person = people.Where(p => p.Positions.Select(m => m.Position).Contains(orgUnitId))
                .OrderBy(o => o.Positions.First(x => x.Position == orgUnitId).Order)
                .FirstOrDefault();

            return person == null ? string.Empty : person.DisplayName;
        }
    }
}
