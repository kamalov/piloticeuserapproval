﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ValidationRules
{
    internal class DecimalOnlyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object oValue, CultureInfo cultureInfo)
        {
            var value = oValue as string;

            if (String.IsNullOrEmpty(value))
                return ValidationResult.ValidResult;

            if (!IsValid(value))
                return new ValidationResult(false, "Please input a correct decimal");

            return ValidationResult.ValidResult;
        }

        public static bool IsValid(string value)
        {
            decimal d;
            return decimal.TryParse(value, NumberStyles.Number, Thread.CurrentThread.CurrentUICulture, out d);
        }
    }
}
