﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ValidationRules
{
    public class LongOnlyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string str = value as string;

            if (!IsValid(str))
                return new ValidationResult(false, "Please enter an integer");

            return ValidationResult.ValidResult;
        }

        public static bool IsValid(string value, out long result)
        {
            if (String.IsNullOrEmpty(value))
            {
                result = 0;
                return true;
            }
            return long.TryParse(value, out result);
        }

        public static bool IsValid(string value)
        {
            long result;
            return IsValid(value, out result);
        }
    }
}
