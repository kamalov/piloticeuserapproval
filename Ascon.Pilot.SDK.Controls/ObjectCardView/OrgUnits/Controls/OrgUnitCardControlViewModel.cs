﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Ascon.Pilot.SDK.Controls.Commands;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{

    public class OrgUnitCardControlViewModel : CardControlViewModel, IOrgUnitsListManager
    {
        private readonly DelegateCommand _showOrgUnitItemsListCommand;
        private readonly DelegateCommand<OrgUnitItemBase> _removeOrgUnitItemCommand;

        public OrgUnitCardControlViewModel(
            IAttribute attribute,
            IType type,
            object initValue,
            bool isReadOnly,
            IPilotDialogService dialogService,
            IObjectsRepository repository,
            IObjectCardAutoComplete autoComplete,
            IAttributeFormatParser attributeFormatParser,
            bool editMode)
            : base(attribute, type, initValue, isReadOnly, dialogService, repository, autoComplete, attributeFormatParser, editMode)
        {
            _showOrgUnitItemsListCommand = new DelegateCommand(SelectExecutor);
            _removeOrgUnitItemCommand = new DelegateCommand<OrgUnitItemBase>(DeleteOrgUnitItem);

            OrgUnits = new ObservableCollection<OrgUnitItemBase>();
            AddNewOrgUnit();
        }

        public ObservableCollection<OrgUnitItemBase> OrgUnits { get; }

        public bool IsReadonly { get; }
        public IRaiseCanExecuteChangedCommand ShowOrgUnitItemsListCommand => _showOrgUnitItemsListCommand;
        public IRaiseCanExecuteChangedCommand RemoveOrgUnitItemCommand => _removeOrgUnitItemCommand;

        private void SelectExecutor()
        {
            var dialogOptions = GetOrgUnitsSelectorDialogOptions();
            var executors = _dialogService.ShowOrganisationUnitSelectorDialog(dialogOptions).Where(x => x.Children.Count == 0).ToList();
            if (!executors.Any())
                return;

            OrgUnits.Clear();
            foreach (var orgUnit in executors)
            {
                OrgUnits.Add(new OrgUnitItem(_repository, this, orgUnit.Id));
            }

            Value = executors.Select(e => e.Id).ToArray();
            AddNewOrgUnit();
        }

        private void DeleteOrgUnitItem(OrgUnitItemBase item)
        {
            OrgUnits.Remove(item);
            if (!OrgUnits.Any())
                AddNewOrgUnit();
        }

        protected virtual IPilotDialogOptions GetOrgUnitsSelectorDialogOptions()
        {
            return _dialogService.NewOptions()
                .WithParentWindow(GetActiveWindow())
                .WithCaption("Select persons")
                .WithOkButtonCaption("Select")
                .WithAllowMultiSelect(true);
        }

        private void AddNewOrgUnit()
        {
            OrgUnits.Add(new NewOrgUnitItem(_repository, this));
        }
    }
}
