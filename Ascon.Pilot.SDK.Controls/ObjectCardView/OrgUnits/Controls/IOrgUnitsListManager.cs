﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls.Commands;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public interface IOrgUnitsListManager
    {
        bool IsReadonly { get; }
        IRaiseCanExecuteChangedCommand ShowOrgUnitItemsListCommand { get; }
        IRaiseCanExecuteChangedCommand RemoveOrgUnitItemCommand { get; }
    }
}
