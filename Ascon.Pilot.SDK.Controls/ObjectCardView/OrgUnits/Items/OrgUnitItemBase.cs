﻿using System;
using System.Collections.Generic;
using Ascon.Pilot.SDK.Controls.Tools;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class OrgUnitItem : OrgUnitItemBase
    {
        public OrgUnitItem(IObjectsRepository repository, IOrgUnitsListManager orgUnitsListManager, int positionId) 
            : base(repository, orgUnitsListManager)
        {
            _positionId = positionId;
            UpdateOrgUnit();
        }
    }

    public class NewOrgUnitItem : OrgUnitItemBase
    {
        public NewOrgUnitItem(IObjectsRepository repository, IOrgUnitsListManager orgUnitsListManager) 
            : base(repository, orgUnitsListManager)
        {
            _title = "New Item";
        }
    }

    public abstract class OrgUnitItemBase : PropertyChangedBase
    {
        protected int _positionId;
        private string _personName;
        protected string _title;

        private bool _isNew;

        protected readonly IObjectsRepository _repository;
        private IOrganisationUnit _organizationUnit;

        protected OrgUnitItemBase(IObjectsRepository repository, IOrgUnitsListManager orgUnitsListManager)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            OrgUnitsListManager = orgUnitsListManager;
        }

        public IOrgUnitsListManager OrgUnitsListManager { get; }

        public virtual string Title
        {
            get => _title;
            protected set
            {
                _title = value;
                NotifyOfPropertyChange(nameof(Title));
            }
        }

        public virtual bool IsNew
        {
            get => _isNew;
            set
            {
                _isNew = value;
                NotifyOfPropertyChange(nameof(IsNew));
            }
        }

        public virtual string PersonName
        {
            get => _personName;
            protected set
            {
                _personName = value;
                NotifyOfPropertyChange(nameof(PersonName));
            }
        }

        public IOrganisationUnit OrganizationUnit
        {
            get => _organizationUnit;
            protected set
            {
                _organizationUnit = value;
                NotifyOfPropertyChange(nameof(OrganizationUnit));
            }
        }

        public virtual int PositionId
        {
            get => _positionId;
            protected set
            {
                _positionId = value;
                NotifyOfPropertyChange(nameof(PositionId));
            }
        }

        protected virtual void UpdateOrgUnit()
        {
            OrganizationUnit = _repository.GetOrganisationUnit(_positionId);
            var person = _repository.GetPersonOnOrganizationUnit(OrganizationUnit.Id);
            Title = OrganizationUnit.GetTitle(person);
            var personName = Title;
            if (person != null)
                personName = person.DisplayName;
            PersonName = personName;
        }

        public override bool Equals(object obj)
        {
            return obj is OrgUnitItemBase other && other.PositionId == PositionId;
        }

        public override int GetHashCode()
        {
            return PositionId.GetHashCode();
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
