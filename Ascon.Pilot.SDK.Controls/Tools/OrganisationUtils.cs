﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.Controls.Tools
{
    public static class OrganizationUtils
    {
        private const string DISPLAY_ID_FORMAT = "D4";

        public static string GetTitle(this IOrganisationUnit organizationUnit, IPerson personOnPosition)
        {
            if (organizationUnit.Kind() != OrganizationUnitKind.Position)
                return organizationUnit.Title;

            if (personOnPosition == null)
                return GetTitle(organizationUnit.Title, organizationUnit.Id);
            return GetTitle(GetActualDisplayName(personOnPosition), organizationUnit.Title);
        }

        public static string GetActualDisplayName(IPerson person)
        {
            return !string.IsNullOrEmpty(person.DisplayName) ? person.DisplayName : person.Login;
        }

        public static string GetTitle(string personName, string organizationUnitName)
        {
            if (string.IsNullOrEmpty(organizationUnitName))
                return personName;

            if (string.IsNullOrEmpty(personName))
                return $"({organizationUnitName})";

            return $"{personName} ({organizationUnitName})";
        }

        public static string GetTitle(string organizationUnitTitle, int organizationUnitId)
        {
            return $"{organizationUnitTitle} - {organizationUnitId.ToString(DISPLAY_ID_FORMAT)}";
        }
    }
}
