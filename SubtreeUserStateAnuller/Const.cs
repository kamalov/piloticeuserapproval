﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubtreeUserStateAnuller
{
    public static class Const
    {
        public const string AnnuledUserState = "state_annulled";
        public const string ApprovedUserState = "approved";
        public const string CreatedUserState = "created";
        public const string OnApprovalUserState = "onApproval";
        public const string DocVerAttrName = "doc_version";
    }
}
