﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Ascon.Pilot.SDK;

namespace SubtreeUserStateAnuller
{
    public static class AttributeExtensions
    {
        public static List<Guid> GetUserStates(this IAttribute attribute)
        {
            try
            {
                var formatter = new XmlSerializer(typeof(UserStateConfiguration));
                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(attribute.Configuration)))
                {
                    var config = (UserStateConfiguration)formatter.Deserialize(stream);
                    return config.UserStates;
                }
            }
            catch (Exception)
            {
                return new List<Guid>();
            }
        }

        public static bool IsUserStateType(this IAttribute attribute)
        {
            return attribute.Type == AttributeType.UserState;
        }
    }
}