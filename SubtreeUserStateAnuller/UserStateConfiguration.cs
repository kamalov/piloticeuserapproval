﻿using System;
using System.Collections.Generic;

namespace SubtreeUserStateAnuller
{
    [Serializable]
    public class UserStateConfiguration
    {
        public List<Guid> UserStates { get; set; }

        public UserStateConfiguration()
        {
        }
    }
}