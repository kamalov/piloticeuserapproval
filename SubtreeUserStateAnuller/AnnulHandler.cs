﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Interop;
using System.Xml.Serialization;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using Microsoft.Win32;
using IDataObject = Ascon.Pilot.SDK.IDataObject;


namespace SubtreeUserStateAnuller
{
    [Export(typeof(IObjectChangeProcessor))]
    public class AnnulHandler : IObjectChangeProcessor, IObserver<INotification>
    {
        public const string Xaml = "<TextBlock Foreground=\"Red\" FontSize=\"20\">АННУЛИРОВАНО</TextBlock>";
        public const string revisionMark = "<TextBlock Foreground=\"Black\" FontFamily=\"Arial\" FontSize=\"20\">ВЕРСИЯ №: ";
        public const string closeTag = "</TextBlock>";
        private readonly string _annuledUserStateId;
        private readonly string _approvedUserStateId;
        private readonly string _onApprovalUserStateId;
        private readonly string _createdUserStateId;
        private readonly string _docVerisonAttrName = "doc_version"; 
        public IObjectsRepository _repository;
        public int _verNumberInt;
        private IPerson _currentPerson;
        private IDisposable _subscription;
        private TaskCompletionSource<IDataObject> _tcs;
        private long _changesetId;
        private readonly IObjectModifier _objectModifier;

        [ImportingConstructor]
        public AnnulHandler(IObjectsRepository repository, IObjectModifier objectModifier)
        {
            _repository = repository;
            var currentUser = repository.GetCurrentPerson().ActualName.ToString();
            var userStates = repository.GetUserStates();
            var annuledUserState = userStates.FirstOrDefault(x => x.Name == Const.AnnuledUserState);
            var approvedUserState = userStates.FirstOrDefault(x => x.Name == Const.ApprovedUserState);
            var createdUserState = userStates.FirstOrDefault(x => x.Name == Const.CreatedUserState);
            var onApprovalUserState = userStates.FirstOrDefault(x => x.Name == Const.OnApprovalUserState);
            if (annuledUserState != null)
                _annuledUserStateId = annuledUserState.Id.ToString();
            if (approvedUserState != null)
                _approvedUserStateId = approvedUserState.Id.ToString();
            if (createdUserState != null)
                _createdUserStateId = createdUserState.Id.ToString();
            //if (onApprovalUserState != null)
            //    _onApprovalUserStateId = onApprovalUserState.Id.ToString();
            var signatureNotifier = repository.SubscribeNotification(NotificationKind.ObjectSignatureChanged);
            var objectNotifier = repository.SubscribeNotification(NotificationKind.ObjectCreated);
            signatureNotifier.Subscribe(this);
            objectNotifier.Subscribe(this);
            _objectModifier = objectModifier;

        }

        public bool changeAtribute(IAttribute sender, AttributeValueChangedEventArgs args, IAttributeModifier modifier) { 
            return true;
        }

        public async void OnNext(INotification value)
        {

            _currentPerson = _repository.GetCurrentPerson();
            if (value.ChangeKind == NotificationKind.ObjectSignatureChanged && _currentPerson.Id == value.UserId)
            {
                var loaderForFirstSign = new ObjectLoader(_repository);
                var obj = await loaderForFirstSign.Load(value.ObjectId);
                Guid approved = new Guid(_approvedUserStateId);

                for (var i = 0; i < obj.Files.Count(); i++)
                {
                    if (obj.Files[i].Signatures.Count() > 0)
                    {
                        for (var j = 0; j < obj.Files[i].Signatures.Count(); j++)
                        {
                            var signature = obj.Files[i].Signatures[j];
                            if (signature.Role == "Утвердил" && signature.Sign != null)
                            {
                                var objBuilder = _objectModifier.Edit(obj);
                                objBuilder.SetAttribute("UserState", approved);
                                _objectModifier.Apply();
                                return;

                            }
                        }
                    }
                }
                return;
            }
             else if (value.ChangeKind == NotificationKind.ObjectCreated && _currentPerson.Id == value.UserId)
            {
                //if (value.TypeId == 25)
                
                    var loaderForCratedObject = new ObjectLoader(_repository);
                    var cobj = await loaderForCratedObject.Load(value.ObjectId);
                    Guid created = new Guid(_createdUserStateId);
                    var objBuilder = _objectModifier.Edit(cobj);
                    objBuilder.SetAttribute("UserState", created);
                    _objectModifier.Apply();
            }
        }

        private string ShowDialog()
        {
            var dialog = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".png",
                Filter = "Изображение подписи (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg"
            };

            if (dialog.ShowDialog() == true)
            {
                return dialog.FileName;
            }
            return "None";
        }


        public bool ProcessChanges(IEnumerable<DataObjectChange> changes, IObjectModifier modifier)
        {
            if (string.IsNullOrEmpty(_annuledUserStateId))
                return true;

            foreach (var change in changes)
            {
                foreach (var attribute in change.New.Attributes)
                {
                    if (Equals(attribute.Value?.ToString(), _annuledUserStateId))
                    {
                        object oldValue = null;
                        change.Old?.Attributes.TryGetValue(attribute.Key, out oldValue);

                        if (!Equals(oldValue, _annuledUserStateId))
                            AddGraphicLayer(change.New, modifier, Xaml);
                    }
                    if (attribute.Key == _docVerisonAttrName)
                    {
                        var label = revisionMark + attribute.Value.ToString() + closeTag;
                        AddGraphicLayer2(change.New, modifier, label, 110, 50);
                    }
                    if (Equals(attribute.Value?.ToString(), _approvedUserStateId))
                    {
                        object oldValue = null;
                        change.New.Attributes.TryGetValue(attribute.Key, out oldValue);
                     
                         foreach (var attr2 in change.New.Attributes)
                         {

                             object revision = null;
                             change.New?.Attributes.TryGetValue(attribute.Key, out revision);
                             if (Equals(revision, "doc_version"))
                             {
                                 System.Windows.MessageBox.Show("Ревизия документа: " + revision.ToString());

                             }
                          }
                        AddGraphicLayer(change.New, modifier, LabelBuilder(_repository));
                         DialogResult result = System.Windows.Forms.MessageBox.Show("Добавить изображение подписи?", "Утверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                         if (result == DialogResult.Yes)
                         {
                            var facsimileFileName = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + ToGuid(_currentPerson.Id);
                            var fileName = ShowDialog();
                            if (fileName != "None")
                            {
                                SaveToDataBaseRastr(change.New, facsimileFileName, modifier, fileName);
                            }
                        }
                    }
                }
            }
            return true;
        }

        private void SaveToDataBaseRastr(IDataObject dataObject, string facsimileFileName, IObjectModifier _modifier, string _filePath)
        {
            if (string.IsNullOrEmpty(_filePath))
                return;
            var builder = _modifier.Edit(dataObject);
            using (var fileStream = File.Open(_filePath, FileMode.Open, FileAccess.ReadWrite))
            {
                var positionId = _currentPerson.MainPosition.Position;
                var byteArray = new byte[fileStream.Length];
                fileStream.Read(byteArray, 0, (int)fileStream.Length);
                var imageStream = new MemoryStream(byteArray);
                var scale = new Point(1, 1);

                var element = Create(300, 50, scale, 0, 0, System.Windows.VerticalAlignment.Bottom,
                    System.Windows.HorizontalAlignment.Left, GraphicLayerElementConstants.BITMAP, ToGuid(_currentPerson.Id), 0, true);
                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(facsimileFileName, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                    builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId, imageStream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
                //_modifier.Apply();
            }
        }
        
        public static GraphicLayerElement Create(double xOffsetFromSettings, double yOffsetFromSettings, Point scale, double angle, int positionId, System.Windows.VerticalAlignment verticalAlignment, System.Windows.HorizontalAlignment horizontalAlignment, string contentType, Guid elementId, int pageNumber, bool isFloating)
        {
            var dpi = 96;
            var xOffset = xOffsetFromSettings / 25.4 * dpi;
            var yOffset = yOffsetFromSettings / 25.4 * dpi;
            var element = new GraphicLayerElement(elementId, Guid.NewGuid(), xOffset, yOffset,
                scale, angle, positionId, pageNumber, verticalAlignment, horizontalAlignment, contentType, isFloating);
            return element;
        }

        public static Guid ToGuid(int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }

        private void AddGraphicLayer2(IDataObject dataObject, IObjectModifier modifier, string label, double coordX,
            double coordY)
        {
            var elementId = Guid.NewGuid();
            var builder = modifier.Edit(dataObject);
            using (var textBlocksStream = new MemoryStream())
            using (var writer = new StreamWriter(textBlocksStream))
            {
                writer.Write(label);
                writer.Flush();

                var name = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + elementId;
                var element = new GraphicLayerElement(
                    elementId,
                    Guid.NewGuid(),
                    coordX,
                    coordY,
                    new Point(1, 1),
                    0,
                    0,
                    0,
                    System.Windows.VerticalAlignment.Bottom,
                    System.Windows.HorizontalAlignment.Left,
                    GraphicLayerElementConstants.XAML,
                    false);

                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(name, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
                builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId, textBlocksStream, DateTime.Now, DateTime.Now, DateTime.Now);
            }
        }

        private void AddGraphicLayer(IDataObject dataObject, IObjectModifier modifier, string Label)
        {
            var elementId = Guid.NewGuid();
            var builder = modifier.Edit(dataObject);
            //var versionCount = VersionCounter(dataObject);
            using (var textBlocksStream = new MemoryStream())
            using (var writer = new StreamWriter(textBlocksStream))
            {
                writer.Write(Label);
                writer.Flush();

                var name = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + elementId;
                var element = new GraphicLayerElement(
                    elementId,
                    Guid.NewGuid(),
                    110,
                    50,
                    new Point(1, 1),
                    0,
                    0,
                    0,
                    System.Windows.VerticalAlignment.Bottom,
                    System.Windows.HorizontalAlignment.Left,
                    GraphicLayerElementConstants.XAML,
                    false);

                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(name, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
                builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId, textBlocksStream, DateTime.Now, DateTime.Now, DateTime.Now);
            }
        }

        private string LabelBuilder(IObjectsRepository repository)
        {
            var user = repository.GetCurrentPerson().ActualName.ToString();
            var timestamp = DateTime.Now.ToString();
            var labelAppr =
                "<TextBlock Foreground=\"Black\" FontFamily=\"Arial\" FontSize =\"20\">УТВЕРЖДЕНО <LineBreak/> " + user + "<LineBreak/>" + timestamp + " </TextBlock>";

            return labelAppr;
        }

     public Task<IDataObject> Load(Guid id, long changesetId = 0)
        {
            _changesetId = changesetId;
            //_tcs = new TaskCompletionSource<IDataObject>();
            _subscription = _repository.SubscribeObjects(new[] { id }).Subscribe((IObserver<IDataObject>)this);
            return _tcs.Task;
        }

        public void OnNext(IDataObject value)
        {
            if (value.State != DataState.Loaded)
                return;

            if (value.LastChange() < _changesetId)
                return;

            _tcs.TrySetResult(value);
            _subscription.Dispose();
        }

        public void OnError(Exception error) { }
        public void OnCompleted() { }


    }
    
}
