﻿using System;
using System.Collections;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using IDataObject = Ascon.Pilot.SDK.IDataObject;

namespace SubtreeUserStateAnuller
{
    [Export(typeof(IObjectCardHandler))]

    public class ObjectCardHandler : IObjectCardHandler
    {
        private const string ControlledAttr = "UserState";
        private IObjectModifier _objectModifier;
        private readonly IObjectsRepository _repository;
        private IUserState myUserState;

        private bool newPrintFlag;
        private int counter;

        [ImportingConstructor]
        public ObjectCardHandler(IObjectModifier objectModifier, IObjectsRepository repository)
        {
            _objectModifier = objectModifier;
            _repository = repository;
            myUserState = _repository.GetUserStates().FirstOrDefault(s => s.Name.Equals(ControlledAttr));
            newPrintFlag = false;

        }

        public bool Handle(IAttributeModifier modifier, ObjectCardContext context)
        {
            counter++;
            if (context.EditiedObject == null) // создание нового объекта
            {
                modifier.SetValue("doc_version", 1);
                newPrintFlag = true;
                return true;
            }
            else
            {
                //работа с существующим объектом
                if (newPrintFlag)
                {
                    // замена.создание новой версии
                    int revisionNumber;
                    var objectCardDataObject = context.EditiedObject;
                    if (objectCardDataObject != null)
                    {
                        revisionNumber = objectCardDataObject.PreviousFileSnapshots.Count + 2;
                        if (objectCardDataObject.PreviousFileSnapshots.Count == 0)
                        {
                            revisionNumber = 2;
                        }
                        modifier.SetValue("doc_version", revisionNumber);
                    }

                    newPrintFlag = (counter % 2) == 1; // выясняем, четный или нечетный раз выполняется метод. Если четный - результат надо применить
                    return true;
                }
                else
                {
                    // открытие карточки существующего объекта
                    counter = 0;
                    newPrintFlag = false;
                    return true;
                }
            }


            /* if (isObjectModification || context.IsReadOnly)
             {
                 int revisionNumber = context.EditiedObject.PreviousFileSnapshots.Count +2;
                 modifier.SetValue("doc_version", revisionNumber);

                 return true;
             }*/
        }

        public bool OnValueChanged(IAttribute sender, AttributeValueChangedEventArgs args, IAttributeModifier modifier)
        {
            newPrintFlag = false;
            if (sender.Name == ControlledAttr | !newPrintFlag)
            {

                return true;
            }

            if (newPrintFlag)
            {
                int revisionNumber;
                var objectCardDataObject = args.Context.EditiedObject;
                if (objectCardDataObject != null)
                {
                    revisionNumber = objectCardDataObject.PreviousFileSnapshots.Count + 1;
                    modifier.SetValue("doc_version", revisionNumber);
                }

                return true;
            }

            if (args.Context == null)
            {
                return true;
            }
            return false;
            // Проверка на изменение версии вручную
            /*if (sender.Name == "doc_version")
            {
                var previousVer = args.OldValue;
                modifier.SetValue("doc_version", args.OldValue.ToString());
                return true;

            }*/

            /*
                if (sender.Name == ControlledAttr)
            {
                var newAttrValue = args.Context.AttributesValues[ControlledAttr];
                var newState = _repository.GetUserStates().FirstOrDefault(s => s.Id.Equals(newAttrValue));


                int revisionNumber;
                var objectCardDataObject  = args.Context.EditiedObject;
                if (objectCardDataObject != null)
                {
                    revisionNumber = objectCardDataObject.PreviousFileSnapshots.Count + 1;
                    modifier.SetValue("doc_version", revisionNumber);
                }
                else
                {
                    revisionNumber = 1;
                    modifier.SetValue("doc_version", revisionNumber);
                }

                // string label = "< TextBlock Foreground =\"Black\" FontFamily=\"Arial\" FontSize =\"20\">" +"ВЕРСИЯ " + revisionNumber.ToString()+ " </TextBlock>";
                //AddGraphicLayer(args.Context.EditiedObject, _objectModifier  , label);
                return true;
             }*/



            /*  var currentAttributeValues = string.Empty;
              foreach (var displayAttribute in args.Context.DisplayAttributes)
              {
                  currentAttributeValues += displayAttribute.Name == sender.Name 
                      ? args.NewValue
                      : displayAttribute.Name + ": " + args.Context.AttributesValues[displayAttribute.Name] + Environment.NewLine;
              }

              if (args.Context.Type.Name == "document" && sender.Name == "sheet_number")
              {
                  var newNameAttrValue = "Sheet no " + args.NewValue + "; " + (args.Context.EditiedObject == null ? " New object " : " Existed object");
                  modifier.SetValue("Name", newNameAttrValue);
                  return true;
              }*/
        }

    }
}