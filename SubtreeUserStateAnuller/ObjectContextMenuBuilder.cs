﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Menu;

namespace SubtreeUserStateAnuller
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    public class ObjectContextMenuBuilder : IMenu<ObjectsViewContext>
    {
        private readonly IObjectsRepository _repository;
        private readonly IObjectModifier _objectModifier;
        private const string AnnulRecursiveName = "miAnnulRecursive";
        
        [ImportingConstructor]
        public ObjectContextMenuBuilder(IObjectsRepository repository, IObjectModifier objectModifier)
        {
            _repository = repository;
            _objectModifier = objectModifier;
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            var objects = context.SelectedObjects.ToList();
            if (objects.Count != 1)
                return;
            var current = objects.Single();
            if (!current.Type.HasFiles)
                return;
        }

        public void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            if (name == AnnulRecursiveName)
            {
                var selectedIds = context.SelectedObjects.Select(x => x.Id).ToList();
                Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                {
                    var userStateAnnuler = new UserStateAnnuler(_repository, _objectModifier);
                    userStateAnnuler.AnnulSubtree(selectedIds);   
                }));
            }
        }
    }
}